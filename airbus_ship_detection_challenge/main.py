import csv
import cv2
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

FILE_NAME = 'airbus_ship_detection_challenge/train_ship_segmentations_v2.csv'
IMAGE_PATH = 'airbus_ship_detection_challenge/images/'
ANNOTATION_PATH = 'airbus_ship_detection_challenge/annotations/'

DATASET = 'Airbus Ship Detection Challenge'
IMAGE_FOLDER_NAME = 'images'
IMAGE_SIZE = 768
IMAGE_DEPTH = 3
SHIP_NAME = 'SHIP'


def read_file():
    image_info = {}
    file_name = open(FILE_NAME)
    reader = csv.reader(file_name, delimiter=',')
    for line in reader:
        if line[0] in image_info:
            # print('Exist image, adding object ...')
            image_info[line[0]].append(line[1])
        else:
            # print('New image, creating object ...')
            objets = [line[1]]
            image_info[line[0]] = objets
    file_name.close()
    return image_info


def main():
    image_info = read_file()
    del image_info['ImageId']
    print(f'Total images: {len(image_info)}')

    for image_key, image_values in image_info.items():
        image_coordinates, image_rectangles = get_objects(image_values)
        create_voc_anotation(image_key, image_rectangles)
        print(f'Image: {image_key}  Objects: {len(image_coordinates)} Rectangles: {len(image_rectangles)}')

        # Plot results
        if image_coordinates and image_rectangles:
            try:
                image = Image.open(f'images/{image_key}')
                image_array_s = np.array(image)
                image_array_s = draw_coordinates(image_array_s, image_coordinates)
                image_array_d = np.array(image)
                image_array_d = draw_rectangles(image_array_d, image_rectangles)

                fig, axs = plt.subplots(1, 3)
                axs[0].set_title('Original image')
                axs[0].imshow(image)
                axs[1].set_title('Segmentation')
                axs[1].imshow(image_array_s)
                axs[2].set_title('Detection')
                axs[2].imshow(image_array_d)

                fig.suptitle(image_key)
                plt.show()
            except Exception as exception:
                print(f'Image not found / Error: {str(exception)}')

    print('Task completed')


def get_objects(image_objects):
    image_coordinates = []
    image_rectangles = []
    for image_object in image_objects:
        object_coordinates = get_object_coordinates(image_object)
        if object_coordinates:
            image_coordinates.append(object_coordinates)
        object_rectangle = get_object_rectangle(image_object)
        if object_rectangle:
            image_rectangles.append(object_rectangle)
    return image_coordinates, image_rectangles


def get_object_coordinates(image_object):
    image_object_value = [int(i) for i in image_object.split()]
    encode_pixel_value = zip(image_object_value[0:-1:2], image_object_value[1::2])
    return [(pixel_position // IMAGE_SIZE, pixel_position % IMAGE_SIZE)
            for start, length in encode_pixel_value
            for pixel_position in range(start, start + length)]


def get_object_rectangle(image_object):
    rectangles = []
    image_object_value = [int(i) for i in image_object.split()]
    encode_pixel_value = zip(image_object_value[0:-1:2], image_object_value[1::2])
    x_values = []
    y_values = []
    for start, length in encode_pixel_value:
        for pixel_position in range(start, start + length):
            x_values.append(pixel_position // IMAGE_SIZE)
            y_values.append(pixel_position % IMAGE_SIZE)
    if x_values:
        min_point = (min(x_values), min(y_values))
        max_point = (max(x_values), max(y_values))
        rectangles.append((min_point, max_point))
    return rectangles


def draw_rectangles(image_array, image_rectangles):
    for image_rectangle in image_rectangles:
        for min_point, max_point in image_rectangle:
            image_array = cv2.rectangle(image_array, min_point, max_point, (0, 255, 0), 2)
    return image_array


def draw_coordinates(image_array, image_coordinates):
    for image_coordinate in image_coordinates:
        for x, y in image_coordinate:
            image_array[y, x, [0, 1, 2]] = 0, 255, 0
    return image_array


def create_voc_anotation(image_id, image_rectangles):
    objects = ''
    image_name = image_id.split('.')
    for image_rectangle in image_rectangles:
        for min_point, max_point in image_rectangle:
            objects += f"""
    <object>
        <name>{SHIP_NAME}</name>
        <pose>Frontal</pose>
        <truncated>0</truncated>
        <difficult>0</difficult>
        <occluded>0</occluded>
        <bndbox>
            <xmin>{min_point[0]}</xmin>
            <xmax>{max_point[0]}</xmax>
            <ymin>{min_point[1]}</ymin>
            <ymax>{max_point[1]}</ymax>
        </bndbox>
    </object>"""
    dataset = 'Airbus Ship Detection Challenge'
    path = f'/{IMAGE_FOLDER_NAME}/{image_id}'
    annotation = f"""<annotation>
    <id>{image_name[0]}</id>
    <folder>{IMAGE_FOLDER_NAME}</folder>
    <filename>{image_id}</filename>
    <path>{path}</path>
    <source>
        <database>{dataset}</database>
    </source>
    <size>
        <width>{IMAGE_SIZE}</width>
        <height>{IMAGE_SIZE}</height>
        <depth>{IMAGE_DEPTH}</depth>
    </size>
    <segmented>0</segmented>{objects}
</annotation>
"""
    f = open(f'{ANNOTATION_PATH}/{image_name[0]}.xml', 'a')
    f.write(annotation)
    f.close()


if __name__ == '__main__':
    main()
