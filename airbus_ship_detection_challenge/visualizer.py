from PIL import Image
import matplotlib.pyplot as plt

IMAGE_PATH = 'C:/Users/William/Documents/Projects/dataset/airbus_ship_detection_challenge/images/'


def main():
    print('Init')
    fig, axs = plt.subplots(2, 3)
    axs[0][0].imshow(Image.open(f'{IMAGE_PATH}/00a52cd2a.jpg'))
    axs[0][1].imshow(Image.open(f'{IMAGE_PATH}/000fd9827.jpg'))
    axs[0][2].imshow(Image.open(f'{IMAGE_PATH}/00a9adaa4.jpg'))
    axs[1][0].imshow(Image.open(f'{IMAGE_PATH}/00a57d4f6.jpg'))
    axs[1][1].imshow(Image.open(f'{IMAGE_PATH}/00a9e2ec9.jpg'))
    axs[1][2].imshow(Image.open(f'{IMAGE_PATH}/000f1f959.jpg'))
    plt.show()


if __name__ == '__main__':
    main()
