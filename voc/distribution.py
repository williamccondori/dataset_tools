import os
import math
import shutil


def fix_distribution(percentage_set, total):
    if not sum(percentage_set) == len(total):
        percentage_set[2] = percentage_set[2] - 1
    if percentage_set[2] == 0:
        percentage_set[2] = 1
        percentage_set[1] = percentage_set[1] - 1
    if percentage_set[1] == 0:
        percentage_set[1] = 1
        percentage_set[0] = percentage_set[0] - 1
    return percentage_set


def split(total, percentage_set):
    train = total[:percentage_set[0]]
    dev = total[percentage_set[0]:percentage_set[0] + percentage_set[1]]
    test = total[percentage_set[0] + percentage_set[1]:percentage_set[0] + percentage_set[1] + percentage_set[2]]
    return [train, dev, test]


def get_data_type_name(index):
    if index == 0:
        return 'train'
    elif index == 1:
        return 'dev'
    else:
        return 'test'


def distribute(annotation_folder, image_folder, output_folder, percentage):
    total = os.listdir(image_folder)
    percentage_set = fix_distribution([math.ceil(
        len(total) * (percentage[i] / 100)) for i in range(len(percentage))], total)
    data_sets = split(total, percentage_set)
    if len(data_sets[0]) + len(data_sets[1]) + len(data_sets[2]) == len(total):
        iterator = 1
        for i in range(0, len(data_sets)):
            data_type = get_data_type_name(i)
            image_path = f'{output_folder}/{data_type}/images'
            annotation_path = f'{output_folder}/{data_type}/annotations'
            os.makedirs(image_path, exist_ok=True)
            os.makedirs(annotation_path, exist_ok=True)

            for data_element in data_sets[i]:
                name = data_element.split('.')[0]
                shutil.copy(f'{image_folder}/{name}.jpg', f'{image_path}/{str(iterator).zfill(7)}.jpg')
                shutil.copy(f'{annotation_folder}/{name}.xml', f'{annotation_path}/{str(iterator).zfill(7)}.xml')
                iterator += 1
        print(f'Train set: {len(data_sets[0])}')
        print(f'Dev set: {len(data_sets[1])}')
        print(f'Test set: {len(data_sets[2])}')
    else:
        print('Incorrect distribution!')


def main():
    annotation_folder = 'C:/Users/William/Documents/Projects/raccoon_dataset/annotations'
    image_folder = 'C:/Users/William/Documents/Projects/raccoon_dataset/images'
    output_folder = 'C:/Users/William/Documents/Racoon'
    percentage = [70, 20, 10]
    distribute(annotation_folder, image_folder, output_folder, percentage)


if __name__ == '__main__':
    main()
