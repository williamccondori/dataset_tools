import math
import os

import matplotlib.pyplot as plt
import numpy as np


def func(percentaje, values):
    result = 0
    absolute = math.ceil(percentaje / 100. * np.sum(values))
    if absolute in values:
        result = absolute
    else:
        absolute += 1
        if absolute in values:
            result = absolute
        else:
            absolute -= 2
            if absolute in values:
                result = absolute
    return f'{percentaje:.1f}%\n({result:d})'


def main():
    path_voc = 'C:/Users/William/Documents/raccoon'
    train_size = len(os.listdir(f'{path_voc}/train/images'))
    dev_size = len(os.listdir(f'{path_voc}/dev/images'))
    test_size = len(os.listdir(f'{path_voc}/test/images'))
    labels = 'Training dataset', 'Development dataset', 'Test dataset'
    values = [train_size, dev_size, test_size]
    explode = (0.1, 0, 0)

    fig1, ax1 = plt.subplots()

    ax1.pie(values, explode=explode, labels=labels, autopct=lambda percentaje: func(percentaje, values),
            shadow=False, startangle=90)
    ax1.axis('equal')
    plt.legend()
    plt.show()


if __name__ == '__main__':
    main()
