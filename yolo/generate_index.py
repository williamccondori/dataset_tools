import os


def main():
    output_path = 'C:/Users/William/Documents/yolo/raccoon'
    input_path = 'C:/Users/William/Documents/yolo/raccoon/train'
    out_file = open(f'{output_path}/train.txt', "w")
    for data_element in sorted(os.listdir(input_path)):
        image = data_element.split('.')
        if image[1] == 'jpg':
            out_file.write(f'{input_path}/{data_element}\n')
    out_file.close()


if __name__ == '__main__':
    main()
