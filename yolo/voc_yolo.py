import os
import shutil
import xml.etree.ElementTree as et


def convert(size, box):
    dw = 1. / size[0]
    dh = 1. / size[1]
    x = (box[0] + box[1]) / 2.0
    y = (box[2] + box[3]) / 2.0
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x * dw
    w = w * dw
    y = y * dh
    h = h * dh
    return x, y, w, h


def convert_annotation(name, images_path, annotation_path, output_path, classes=None):
    in_file = open(f'{annotation_path}/{name}.xml')
    out_file = open(f'{output_path}/{name}.txt', "w")

    tree = et.parse(in_file)
    root = tree.getroot()
    size = root.find('size')
    w = int(size.find('width').text)
    h = int(size.find('height').text)

    for classs in classes:
        for obj in root.iter('object'):
            class_name = obj.find('name').text
            if class_name == classs:
                class_id = str(classes.index(class_name))
                box = obj.find('bndbox')
                bounding_box = (float(box.find('xmin').text), float(box.find('xmax').text), float(
                    box.find('ymin').text), float(box.find('ymax').text))
                coordinates = convert((w, h), bounding_box)
                out_file.write(f'{class_id} {coordinates[0]} {coordinates[1]} {coordinates[2]} {coordinates[3]}\n')
                shutil.copy(f'{images_path}/{name}.jpg', f'{output_path}/{name}.jpg')
    out_file.close()


def main():
    images_path = 'C:/Users/William/Documents/raccoon/train/images'
    annotation_path = 'C:/Users/William/Documents/raccoon/train/annotations'
    output_path = 'C:/Users/William/Documents/yolo/raccoon/train'
    for data_element in sorted(os.listdir(annotation_path)):
        name = data_element.split('.')[0]
        convert_annotation(name, images_path, annotation_path, output_path, ['raccoon'])


if __name__ == '__main__':
    main()
